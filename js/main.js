const swiper = new Swiper(".swiper", {
  autoplay: {
    delay: 7000,
  },

  pagination: {
    el: ".auto-slider__pagination",
    bulletClass: "auto-slider__pagination-bullet",
    bulletActiveClass: "auto-slider__pagination-bullet--active",
    clickable: true,
  },

  watchOverflow: true,

  /* The code you provided is configuring the Swiper library to create a slideshow with responsive
  behavior. */
  slidesPerView: 1,
  spaceBetween: 14,
  breakpoints: {
    // when window width is >= 410px
    410: {
      slidesPerView: 2,
    },
    // when window width is >= 800px
    800: {
      slidesPerView: 3,
    },
  },
});
